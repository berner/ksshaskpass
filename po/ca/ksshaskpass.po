# Translation of ksshaskpass.po to Catalan
# Copyright (C) 2015-2023 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2015, 2020, 2021, 2022, 2023.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2017, 2020.
# Alfredo Vicente <alviboi@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: ksshaskpass\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-05 02:51+0000\n"
"PO-Revision-Date: 2023-05-16 14:26+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 20.12.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Josep M. Ferrer"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "txemaq@gmail.com"

#: main.cpp:244 main.cpp:313 main.cpp:336
#, kde-format
msgid "Ksshaskpass"
msgstr "Ksshaskpass"

#: main.cpp:246
#, kde-format
msgid "KDE version of ssh-askpass"
msgstr "Versió del «ssh-askpass» del KDE"

#: main.cpp:248
#, kde-format
msgid ""
"(c) 2006 Hans van Leeuwen\n"
"(c) 2008-2010 Armin Berres\n"
"(c) 2013 Pali Rohár"
msgstr ""
"(c) 2006 Hans van Leeuwen\n"
"(c) 2008-2010 Armin Berres\n"
"(c) 2013 Pali Rohár"

#: main.cpp:249
#, kde-format
msgid ""
"Ksshaskpass allows you to interactively prompt users for a passphrase for "
"ssh-add"
msgstr ""
"El Ksshaskpass permet demanar interactivament als usuaris una frase de pas "
"per al «ssh-add»"

#: main.cpp:253
#, kde-format
msgid "Armin Berres"
msgstr "Armin Berres"

#: main.cpp:253
#, kde-format
msgid "Current author"
msgstr "Autor actual"

#: main.cpp:254
#, kde-format
msgid "Hans van Leeuwen"
msgstr "Hans van Leeuwen"

#: main.cpp:254
#, kde-format
msgid "Original author"
msgstr "Autor original"

#: main.cpp:255
#, kde-format
msgid "Pali Rohár"
msgstr "Pali Rohár"

#: main.cpp:255
#, kde-format
msgid "Contributor"
msgstr "Col·laborador"

#: main.cpp:260
#, kde-format
msgctxt "Name of a prompt for a password"
msgid "Prompt"
msgstr "Indicació"

#: main.cpp:266
#, kde-format
msgid "Please enter passphrase"
msgstr "Introduïu la frase de pas"

#: main.cpp:314
#, kde-format
msgctxt "@action:button"
msgid "Accept"
msgstr "Accepta"
