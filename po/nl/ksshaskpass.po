# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2015, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-05 02:51+0000\n"
"PO-Revision-Date: 2022-12-07 11:14+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.08.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Freek de Kruijf - t/m 2022"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "freekdekruijf@kde.nl"

#: main.cpp:244 main.cpp:313 main.cpp:336
#, kde-format
msgid "Ksshaskpass"
msgstr "Ksshaskpass"

#: main.cpp:246
#, kde-format
msgid "KDE version of ssh-askpass"
msgstr "KDE versie van ssh-askpass"

#: main.cpp:248
#, kde-format
msgid ""
"(c) 2006 Hans van Leeuwen\n"
"(c) 2008-2010 Armin Berres\n"
"(c) 2013 Pali Rohár"
msgstr ""
"(c) 2006 Hans van Leeuwen\n"
"(c) 2008-2010 Armin Berres\n"
"(c) 2013 Pali Rohár"

#: main.cpp:249
#, kde-format
msgid ""
"Ksshaskpass allows you to interactively prompt users for a passphrase for "
"ssh-add"
msgstr ""
"Ksshaskpass stelt u in staat om gebruikers interactief te vragen naar een "
"wachtzin voor ssh-add"

#: main.cpp:253
#, kde-format
msgid "Armin Berres"
msgstr "Armin Berres"

#: main.cpp:253
#, kde-format
msgid "Current author"
msgstr "Huidige auteur"

#: main.cpp:254
#, kde-format
msgid "Hans van Leeuwen"
msgstr "Hans van Leeuwen"

#: main.cpp:254
#, kde-format
msgid "Original author"
msgstr "Oorspronkelijke auteur"

#: main.cpp:255
#, kde-format
msgid "Pali Rohár"
msgstr "Pali Rohár"

#: main.cpp:255
#, kde-format
msgid "Contributor"
msgstr "Medewerker"

#: main.cpp:260
#, kde-format
msgctxt "Name of a prompt for a password"
msgid "Prompt"
msgstr "Prompt"

#: main.cpp:266
#, kde-format
msgid "Please enter passphrase"
msgstr "Voer wachtzin in"

#: main.cpp:314
#, kde-format
msgctxt "@action:button"
msgid "Accept"
msgstr "Accepteren"
