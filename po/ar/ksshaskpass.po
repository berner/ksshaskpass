# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Safa Alfulaij <safa1996alfulaij@gmail.com>, 2015.
# Zayed Al-Saidi <zayed.alsaidi@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-05 02:51+0000\n"
"PO-Revision-Date: 2022-12-22 11:08+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: ar\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 21.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "صفا الفليج"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "safa1996alfulaij@gmail.com"

#: main.cpp:244 main.cpp:313 main.cpp:336
#, kde-format
msgid "Ksshaskpass"
msgstr "Ksshaskpass"

#: main.cpp:246
#, kde-format
msgid "KDE version of ssh-askpass"
msgstr "إصدارة كِيدِي من ssh-askpass"

#: main.cpp:248
#, kde-format
msgid ""
"(c) 2006 Hans van Leeuwen\n"
"(c) 2008-2010 Armin Berres\n"
"(c) 2013 Pali Rohár"
msgstr ""
"(c) 2006 Hans van Leeuwen\n"
"(c) 2008-2010 Armin Berres\n"
"(c) 2013 Pali Rohár"

#: main.cpp:249
#, kde-format
msgid ""
"Ksshaskpass allows you to interactively prompt users for a passphrase for "
"ssh-add"
msgstr "يسمح Ksshaskpass لك بطلب عبارة مرور ssh-add من المستخدمين بشكل تفاعليّ"

#: main.cpp:253
#, kde-format
msgid "Armin Berres"
msgstr "Armin Berres"

#: main.cpp:253
#, kde-format
msgid "Current author"
msgstr "المؤلّف الحاليّ"

#: main.cpp:254
#, kde-format
msgid "Hans van Leeuwen"
msgstr "Hans van Leeuwen"

#: main.cpp:254
#, kde-format
msgid "Original author"
msgstr "المؤلّف الأصليّ"

#: main.cpp:255
#, kde-format
msgid "Pali Rohár"
msgstr "Pali Rohár"

#: main.cpp:255
#, kde-format
msgid "Contributor"
msgstr "مساهم"

#: main.cpp:260
#, kde-format
msgctxt "Name of a prompt for a password"
msgid "Prompt"
msgstr "اطلب عبارة المرور"

#: main.cpp:266
#, kde-format
msgid "Please enter passphrase"
msgstr "فضلًا أدخل عبارة المرور"

#: main.cpp:314
#, kde-format
msgctxt "@action:button"
msgid "Accept"
msgstr "اقبل"
